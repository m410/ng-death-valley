/*
 * Public API Surface of ng-death-valley
 */

export * from './lib/ng-death-valley.service';
export * from './lib/ng-death-valley.module';
